package com.jayden.advanced.algorithm.offer;

/**
 *    [剑指 Offer 04]二维数组中的查找
 * //在一个 n * m 的二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个高效的函数，输入这样的一个二维数组和一个
 * //整数，判断数组中是否含有该整数。
 * //
 * //
 * //
 * // 示例:
 * //
 * // 现有矩阵 matrix 如下：
 * //
 * //
 * //[
 * //  [1,   4,  7, 11, 15],
 * //  [2,   5,  8, 12, 19],
 * //  [3,   6,  9, 16, 22],
 * //  [10, 13, 14, 17, 24],
 * //  [18, 21, 23, 26, 30]
 * //]
 * //
 * //
 * // 给定 target = 5，返回 true。
 * //
 * // 给定 target = 20，返回 false。
 *
 * @author by laijie
 * @Date 2021/4/1 13:55
 */
public class Of04 {


    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {1,   4,  7, 11, 15},
                {2,   5,  8, 12, 19},
                {3,   6,  9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30}};

        int num = 25;
        boolean numberIn2DArrayOne = findNumberIn2DArrayOne(matrix, num);
        boolean numberIn2DArrayOne1 = findNumberIn2DArrayOne(matrix, num);
        System.out.println(numberIn2DArrayOne);
        System.out.println(numberIn2DArrayOne1);
    }

    /**
     * 一个一个遍历
     * - 时间复杂度：*O(m * n)*
     * - 空间复杂度：*O(1)*
     */
    public static boolean findNumberIn2DArrayOne(int[][] matrix, int target) {

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j <matrix[0].length; j++) {
                if(matrix[i][j] == target){
                    return true;
                }

            }
        }

        return false;
    }

    /**
     * 线性遍历
     * - 时间复杂度：*O(m + n)*
     * - 空间复杂度：*O(1)*
     *
     * @param matrix
     * @param target
     * @return
     */
    public static boolean findNumberIn2DArrayTwo(int[][] matrix, int target) {

        int col =matrix[0].length - 1;
        int row = 0;

        while (col >= 0 && row < matrix.length){
            int ele = matrix[col][row];
            if(ele == target){
                return true;
            }

            if(ele > target){
                col--;
            }else{
                row++;
            }
        }

        return false;
    }






















}
