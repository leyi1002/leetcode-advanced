package com.jayden.advanced.algorithm.offer;

import java.util.ArrayList;
import java.util.Stack;

/**
 * [剑指 Offer 06]从尾到头打印链表
 * <p>
 * 输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。
 * // 示例 1：
 * //
 * // 输入：head = [1,3,2]
 * //输出：[2,3,1]
 *
 * @author by laijie
 * @Date 2021/4/1 15:59
 */
public class Of06 {

    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(3);
        ListNode listNode3 = new ListNode(2);
        listNode1.next = listNode2;
        listNode2.next = listNode3;

        int[] ints = reversePrintOne(listNode1);
        int[] ints2 = reversePrintTwo(listNode1);


        for (int i = 0; i < ints.length; i++) {
            System.out.print(ints[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < ints.length; i++) {
            System.out.print(ints2[i] + " ");
        }

    }

    public static int[] reversePrintOne(ListNode head) {

        ArrayList<Integer> list = new ArrayList<>();

        ListNode next = head;
        while (next != null){
            list.add(next.val);
            next = next.next;
        }

        int size = list.size();
        int[] ints = new int[size];
        for (int i = size - 1; i >= 0; i--) {
            ints[size-i-1] = list.get(i);
        }
        return ints;
    }


    public static int[] reversePrintTwo(ListNode head){
        Stack<Integer> stack = new Stack<>();

        ListNode next = head;
        while (next != null){
            stack.push(next.val);
            next = next.next;
        }

        int[] ints = new int[stack.size()];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = stack.pop();
        }
        return ints;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
