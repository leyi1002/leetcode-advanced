package com.jayden.advanced.algorithm.offer;

import java.util.HashMap;
import java.util.Map;

/**
 * [剑指 Offer 07]重建二叉树
 * <p>
 * //输入某二叉树的前序遍历和中序遍历的结果，请重建该二叉树。假设输入的前序遍历和中序遍历的结果中都不含重复的数字。
 *
 * @author by laijie
 * @Date 2021/4/1 16:19
 */
public class Of07 {



    public static void main(String[] args) {
        int[] preorder = new int[]{3,9,20,15,7};
        int[] inorder = new int[]{9,3,15,20,7};

        TreeNode treeNode = buildTree(preorder, inorder);
        System.out.println(treeNode);

    }

    private static Map<Integer, Integer> map = new HashMap<>();

    public static TreeNode buildTree(int[] preorder, int[] inorder) {
        for (int i = 0; i < inorder.length; i++) {
            map.put(preorder[i], i);
        }

        return recurive(0, 0, preorder.length - 1, preorder);
    }

    private static TreeNode recurive(int preoorderRootIndex, int left, int right, int[] preorder) {
        if (left > right) {
            return null;
        }
        TreeNode root = new TreeNode(preorder[preoorderRootIndex]);

        Integer inorderRootIndex = map.get(preorder[preoorderRootIndex]);

        root.left = recurive(preoorderRootIndex + 1, left, inorderRootIndex - 1, preorder);
        root.right = recurive(preoorderRootIndex + inorderRootIndex - left + 1, inorderRootIndex + 1, right, preorder);

        return root;
    }

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}


