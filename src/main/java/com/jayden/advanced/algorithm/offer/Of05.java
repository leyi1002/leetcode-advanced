package com.jayden.advanced.algorithm.offer;

/**
 *    [剑指 Offer 05]替换空格
 * 请实现一个函数，把字符串 s 中的每个空格替换成"%20"。
 * // 示例 1：
 * //
 * // 输入：s = "We are happy."
 * // 输出："We%20are%20happy."
 *
 * // 限制：
 * //
 * // 0 <= s 的长度 <= 10000
 * @author by laijie
 * @Date 2021/4/1 14:10
 */
public class Of05 {

    public static void main(String[] args) {
        String tx = "We are happy.";
        System.out.println(replaceSpaceOne(tx));
        System.out.println(replaceSpaceTwo(tx));
    }

    private static String replaceSpaceOne(String s){
        return s.replaceAll(" ", "%20");
    }

    private static String replaceSpaceTwo(String s){
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == ' '){
                stringBuffer.append("%20");
            }else {
                stringBuffer.append(s.charAt(i));
            }
        }
        return stringBuffer.toString();
    }
}
