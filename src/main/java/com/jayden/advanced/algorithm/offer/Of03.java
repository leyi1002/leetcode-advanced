package com.jayden.advanced.algorithm.offer;

import java.util.HashMap;

/**
 * 找出数组中重复的数字
 * n 的数组 nums 里的所有数字都在 0～n-1的范围内。数组中某些数字是重复的，但不知道有几个数字重复了，
 * 也不知道每个数字重复了几次。请找出数组中任意一个重复的数字。
 *
 * @author by laijie
 * @Date 2021/3/31 17:22
 */
public class Of03 {

    public static void main(String[] args) {
//        int[] nums = new int[]{2, 3, 4, 4, 0, 5, 6};
        int[] nums = new int[]{1, 2, 3,3,5,0};
        System.out.println(findOne(nums));
        System.out.println(findTwo(nums));
    }


    private static int findOne(int[] nums){
        HashMap<Object, Object> hashMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            if(hashMap.containsKey(nums[i])){
                return nums[i];
            }
            hashMap.put(nums[i], 0);
        }
        return -1;
    }

    private static int findTwo(int[] nums){

        for (int i = 0; i < nums.length; i++) {

            while (nums[i] != i){
                if(nums[nums[i]] == nums[i]){
                    return nums[i];
                }

                int temp = nums[nums[i]];
                nums[nums[i]] = nums[i];
                nums[i] = temp;
            }
        }

        return -1;
    }
}
