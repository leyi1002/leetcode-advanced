package com.jayden.advanced.algorithm.subject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * 21. 合并两个有序链表
 *
 * @author by laijie
 * @Date 2021/4/21 下午4:24
 */
public class S21 {

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if(l1 == null){
            return l2;
        }
        if(l2 == null){
            return l1;
        }
        if(l1.val < l2.val){
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        }else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }



    }
}
