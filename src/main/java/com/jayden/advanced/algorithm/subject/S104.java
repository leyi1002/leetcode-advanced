package com.jayden.advanced.algorithm.subject;

/**
 * 104. 二叉树的最大深度
 *
 * @author by laijie
 * @Date 2021/4/21 下午5:23
 */
public class S104 {

    public int maxDepth(TreeNode root) {
        if(root == null){
            return 0;
        }

        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }
}
