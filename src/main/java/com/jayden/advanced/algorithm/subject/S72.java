package com.jayden.advanced.algorithm.subject;

/**
 * 72. 编辑距离，动态规划
 *
 * @author by laijie
 * @Date 2021/4/22 下午4:26
 */
public class S72 {

    public static void main(String[] args) {
        S72 s72 = new S72();
        int i = s72.minDistance("abc", "dooop");
        System.out.println(i);

    }

    private int min(int a, int b, int c) {
        return Math.min(a, Math.min(b, c));
    }

    public int minDistance(String word1, String word2) {

        int[][] table = new int[word1.length() + 1][word2.length() + 1];

        for (int col = 1; col < table[0].length; col++) {
            table[0][col]=col;
        }

        for (int row = 1; row < table.length; row++) {
            table[row][0] = row;
        }

        for (int row = 1; row < table.length; row++) {
            char char1 = word1.charAt(row - 1);
            for (int col = 1; col < table[row].length; col++) {
                char char2 = word2.charAt(col - 1);
                if(char1 == char2){
                    table[row][col] = table[row-1][col-1];
                }else {
                    table[row][col] = min(table[row-1][col-1], table[row][col-1], table[row-1][col]) + 1;
                }
            }
        }
        return table[table.length-1][table[table.length-1].length -1];
    }

}
