package com.jayden.advanced.algorithm.subject;

import java.util.HashMap;

/**
 * 146. LRU 缓存机制
 *
 * @author by laijie
 * @Date 2021/4/22 下午9:52
 */
public class S146 {




    public static void main(String[] args) {

    }

    public S146(int capacity) {
        this.size = 0;
        this.capacity = capacity;
        this.head = new DlinkedNode(0,0);
        this.tail = new DlinkedNode(0,0);
        this.head.next = tail;
        this.tail.prev = head;
    }

    public int get(int key) {
        DlinkedNode node = cache.get(key);
        if(node == null){
            return -1;
        }
        moveToHead(node);
        return node.value;
    }

    private void moveToHead(DlinkedNode node) {
        removeNode(node);
        addToHead(node);
    }

    private void addToHead(DlinkedNode node) {
        head.next.prev = node;
        node.next = head.next;
        head.next = node;
        node.prev = head;
    }

    private void removeNode(DlinkedNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    private DlinkedNode removeTail() {
        DlinkedNode prev = tail.prev;
        removeNode(prev);
        return prev;
    }

    public void put(int key, int value) {
        DlinkedNode node = cache.get(key);
        if(node == null){
            DlinkedNode newNode = new DlinkedNode(key, value);
            cache.put(key, newNode);
            addToHead(newNode);
            size++;
            if(size > capacity){
                DlinkedNode removed = removeTail();
                cache.remove(removed.key);
                size--;
            }

        }else {
            node.value = value;
            moveToHead(node);
        }
    }


    private DlinkedNode head;
    private DlinkedNode tail;
    private HashMap<Integer,DlinkedNode> cache = new HashMap<>();
    private int size;
    private int capacity;

    class DlinkedNode{
        int key;
        int value;
        DlinkedNode prev;
        DlinkedNode next;

        public DlinkedNode() {
        }
        public DlinkedNode(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }



}
