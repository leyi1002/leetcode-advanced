package com.jayden.advanced.algorithm.subject;

/**
 * 5. 最长回文子串
 *
 * @author by laijie
 * @Date 2021/4/20 23:00
 */
public class S5 {

    public static void main(String[] args) {
        String text = "babad";
        S5 s5 = new S5();
        String s = s5.longestPalindrome2(text);
        System.out.println(s);
    }

    public String longestPalindrome(String s) {
        if(s == null){
            return null;
        }
        int length = s.length();
        if(length < 2){
            return s;
        }
        int begin = 0;
        int maxLength = 0;
        boolean[][] dp = new boolean[length][length];

        for (int j = 0; j < length; j++) {

            for (int i = 0; i <= j; i++) {
                if(i == j){
                    dp[i][j] = true;
                }else if(s.charAt(i) != s.charAt(j)){
                    dp[i][j] = false;
                }else {
                    if(j - i < 3){
                        dp[i][j] = true;
                    }else {
                        dp[i][j] = dp[i+1][j-1];
                    }
                }
                if(dp[i][j] && j - i + 1 >maxLength){
                    maxLength = j - i + 1;
                    begin = i;
                }
            }
        }
        return s.substring(begin, begin  + maxLength);


    }


    /**
     * 中心拓展算法
     *
     * @param s
     * @return
     */
    public String longestPalindrome2(String s) {
        if(s == null){
            return null;
        }
        int length = s.length();
        if(length < 2){
            return s;
        }

        int begin = 0;
        int maxLength = 0;


        for (int i = 0; i < length; i++) {

            int lengOdd = expendCenter(s, i, i);
            int lengEven = expendCenter(s, i, i + 1);
            int curLeng = Math.max(lengOdd, lengEven);

            if(curLeng > maxLength){
                begin = i - (curLeng - 1) / 2;
                maxLength = curLeng;
            }
        }
        return s.substring(begin, begin + maxLength);

    }

    private int expendCenter(String s, int left, int right) {
        while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)){
            left--;
            right++;
        }
        return right - left - 1;
    }
}
