package com.jayden.advanced.algorithm.subject;

import java.util.ArrayList;
import java.util.List;

/**
 * 54. 螺旋矩阵
 *
 * @author by laijie
 * @Date 2021/4/22 下午8:48
 */
public class S54 {

    public static void main(String[] args) {
//        int[][] num = new int[][]{{1,2,3},{4,5,6},{7,8,9}};
        int[][] num = new int[][]{{3,2,3}};

        S54 s54 = new S54();
        List<Integer> integers = s54.spiralOrder(num);
        System.out.println(integers);

    }


    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> order = new ArrayList<>();
        if(matrix == null || matrix.length == 0 || matrix[0].length == 0){
            return order;
        }

        int left = 0,right=matrix[0].length-1,top=0,bottem=matrix.length-1;

        while (left <= right && top <= bottem ){

            for (int col = left; col <= right; col++) {
                order.add(matrix[top][col]);
            }

            for (int row = top + 1; row <= bottem; row++) {
                order.add(matrix[row][right]);
            }


            if(bottem > top){
                for (int col = right - 1 ; col > left ; col--) {
                    order.add(matrix[bottem][col]);
                }
            }

            if(left < right){
                for (int row = bottem ; row > top; row--) {
                    order.add(matrix[row][left]);
                }
            }


            left++;
            right--;
            top++;
            bottem--;
        }
        return order;
    }
}
