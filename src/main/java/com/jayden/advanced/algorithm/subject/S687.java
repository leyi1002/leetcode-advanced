package com.jayden.advanced.algorithm.subject;

/**
 * 687. 最长同值路径
 *
 * @author by laijie
 * @Date 2021/4/20 10:40
 */
public class S687 {

    private int maxValue = 0;

    public int longestUnivaluePath(TreeNode root) {
        maxValue(root);
        return maxValue;
    }

    public int maxValue(TreeNode node){

        if(node.left == null && node.right == null){
            return 0;
        }

        int leftValue = node.left == null ? 0 : maxValue(node.left) + 1;
        int rightValue = node.right == null ? 0 : maxValue(node.right) + 1;

        if(node.left != null && node.left.val != node.val){
            leftValue = 0;
        }
        if(node.right != null && node.right.val != node.val){
            rightValue = 0;
        }

        maxValue = Math.max(leftValue + rightValue, maxValue);

        return leftValue + rightValue;


    }



}
