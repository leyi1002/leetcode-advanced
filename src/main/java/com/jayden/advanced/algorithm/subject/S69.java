package com.jayden.advanced.algorithm.subject;

/**
 * 69. x 的平方根
 *
 * @author by laijie
 * @Date 2021/4/22 下午1:35
 */
public class S69 {
    public static void main(String[] args) {
        S69 s69 = new S69();
        int i = s69.mySqrt(2147395599);
        System.out.println(i);
    }

    public int mySqrt(int x) {
        int left = 0, right = x;

        while (left < right){
            int mid = left + (right -left + 1) / 2;
            if(mid * mid <= x){
                left = mid;
            }else {
                right = mid -1;
            }
        }
        return left;
    }

}
