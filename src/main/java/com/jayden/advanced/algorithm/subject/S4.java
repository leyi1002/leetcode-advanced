package com.jayden.advanced.algorithm.subject;

/**
 * 4. 寻找两个正序数组的中位数
 *
 * @author by laijie
 * @Date 2021/4/22 下午5:33
 */
public class S4 {

    public static void main(String[] args) {
        S4 s4 = new S4();
        int[] nums1 =new int[] {1,3};
        int[] nums2 =new int[] {2,5,6};

        int[] medianSortedArrays = s4.merge(nums1, nums2);


        System.out.println(medianSortedArrays);
    }

    public int[] merge(int[] nums1, int[] nums2){
        int length = nums1.length + nums2.length;
        int[] ints = new int[length];
        int i=0,j=0,count=0;

        while (count != length){
            if(i == nums1.length){
                while (j != nums2.length){
                    ints[count++] = nums2[j++];
                }
                break;
            }
            if(j == nums2.length){
                while (i != nums1.length){
                    ints[count++] = nums1[i++];
                }
                break;
            }
            if(nums1[i] > nums2[j]){
                ints[count++] = nums2[j++];
            }else {
                ints[count++] = nums1[i++];
            }
        }

        return ints;

    }
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {



        int[] merge = merge(nums1, nums2);

        int length = merge.length;
        if(length % 2 == 0){
            return (merge[length/2] + merge[length/2 - 1])/2.0;
        }else {
            return merge[length/2];
        }

    }


}
