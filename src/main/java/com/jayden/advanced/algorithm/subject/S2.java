package com.jayden.advanced.algorithm.subject;

/**
 * 2. 两数相加
 *
 * @author by laijie
 * @Date 2021/4/20 21:18
 */
public class S2 {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode pre = new ListNode(0);
        ListNode cur = pre;

        int sum = 0 ,carry = 0;


        while (l1 != null || l2 != null){

            int l1Value = l1 == null ? 0 : l1.val;
            int l2Value = l2 == null ? 0 : l2.val;

            sum = l1Value + l2Value + carry;

            carry = sum / 10;
            sum = sum %10;
            cur.next = new ListNode(sum);

            cur = cur.next;

            if(l1 != null){
                l1 = l1.next;
            }

            if(l2 != null){
                l2 = l2.next;
            }
        }

        if(carry == 1){
            cur.next = new ListNode(carry);
        }

        return pre.next;
    }
}
