package com.jayden.advanced.algorithm.subject;

/**
 * 24. 两两交换链表中的节点
 *
 * @author by laijie
 * @Date 2021/4/21 下午5:28
 */
public class S24 {

    public ListNode swapPairs(ListNode head) {
        if(head == null || head.next == null){
            return head;
        }
        ListNode next = head.next;

        next.next = head;
        head.next = swapPairs(next.next);

        return next;
    }
}
