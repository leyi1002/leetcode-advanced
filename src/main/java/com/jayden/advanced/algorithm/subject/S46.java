package com.jayden.advanced.algorithm.subject;

import java.util.*;

/**
 * 46. 全排列
 *
 * @author by laijie
 * @Date 2021/4/21 下午9:29
 */
public class S46 {

    public static void main(String[] args) {
        int[] nums = new int[]{1,2,3};
        S46 s46 = new S46();
        List<List<Integer>> permute = s46.permute(nums);
        System.out.println(permute);

    }

    public List<List<Integer>> permute(int[] nums) {

        List<List<Integer>> res = new ArrayList<>();
        int length = nums.length;
        if(nums.length == 0){
            return res;
        }

        boolean[] used = new boolean[length];
        LinkedList<Integer> linkedList = new LinkedList<>();

        dfs(nums, length, 0, linkedList, used, res);

        return res;
    }

    private void dfs(int[] nums, int length, int depth, LinkedList<Integer> linkedList, boolean[] used, List<List<Integer>> res) {
        if(depth == length){
            res.add(new LinkedList<>(linkedList));
            return;
        }
        for (int i = 0; i < length; i++) {
            if(!used[i]){
                used[i] = true;
                linkedList.addLast(nums[i]);
                dfs(nums, length, depth + 1, linkedList, used, res);
                linkedList.removeLast();
                used[i] = false;
            }
        }

    }

}
