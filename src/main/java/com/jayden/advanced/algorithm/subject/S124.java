package com.jayden.advanced.algorithm.subject;

/**
 * 124. 二叉树中的最大路径和
 *
 * @author by laijie
 * @Date 2021/4/20 10:11
 */
public class S124 {

    private int sum = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {

        maxSum(root);
        return sum;
    }

    public int maxSum(TreeNode node) {
        if (node == null) {
            return 0;
        }

        int leftSum = Math.max(maxSum(node.left), 0);
        int rightSum = Math.max(maxSum(node.right), 0);

        sum = Math.max(leftSum + rightSum + node.val, sum);

        return Math.max(leftSum, rightSum) + node.val;
    }

}
