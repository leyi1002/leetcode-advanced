package com.jayden.advanced.algorithm.subject;

/**
 * 53. 最大子序和
 *
 * @author by laijie
 * @Date 2021/4/21 下午10:25
 */
public class S53 {

    public static void main(String[] args) {
        int[] nums = new int[]{5,4,-1,7,8};
        S53 s53 = new S53();
        int i = s53.maxSubArray(nums);
        System.out.println(i);
    }

    public int maxSubArray(int[] nums) {

        int pre = 0,max = nums[0];
        for (int x: nums){
            pre = Math.max(pre + x, x);
            max = Math.max(pre, max);
        }

        return max;
    }

    public int maxSubArray1(int[] nums) {
        int length = nums.length;
        int max = nums[0];
        for (int i = 0; i < length; i++) {
            int sum =0;
            for (int j = i; j < length ; j++) {
                sum += nums[j];
                if(sum > max){
                    max = sum;
                }
            }
            
        }
        return max;
    }
}
