package com.jayden.advanced.algorithm.subject;

/**
 * 344. 反转字符串
 *
 * @author by laijie
 * @Date 2021/4/21 下午4:41
 */
public class S344 {


    public void reverseString(char[] s) {
        for (int i = 0; i < s.length / 2; i++) {
            int index = s.length - 1 - i;
            char temp = s[i];
            s[i] = s[index];
            s[index] = temp;
        }

    }
}
