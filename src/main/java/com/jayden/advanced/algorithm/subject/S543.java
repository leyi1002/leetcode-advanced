package com.jayden.advanced.algorithm.subject;


/**
 * 543. 二叉树的直径
 */
public class S543 {

    private static int maxHeight = 0;

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        treeNode.right = treeNode2;

        int i = diameterOfBinaryTree(treeNode);
        System.out.println(i);
    }

    private static int diameterOfBinaryTree(TreeNode root) {

        getMaxHeight(root);

        return maxHeight;
    }

    private static int getMaxHeight(TreeNode node) {
        if (node.left == null && node.right == null) {
            return 0;
        }

        int leftHeight = node.left == null ? 0 : getMaxHeight(node.left) + 1;
        int rightHeight = node.right == null ? 0 : getMaxHeight(node.right) + 1;

        maxHeight = Math.max(leftHeight + rightHeight, maxHeight);

        return Math.max(leftHeight, rightHeight);

    }

}



