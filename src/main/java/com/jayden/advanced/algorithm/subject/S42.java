package com.jayden.advanced.algorithm.subject;


/**
 * 42. 接雨水
 * 动态规划
 *
 * @author by laijie
 * @Date 2021/4/23 下午5:47
 */
public class S42 {

    public static void main(String[] args) {
//        int[] nums = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
        int[] nums = {4,2,0,3,2,5};
        S42 s42 = new S42();
        int trap = s42.trap(nums);
        System.out.println(trap);
    }

    public int trap(int[] height) {
        int length = height.length;
        int[] leftMax = new int[length];
        int[] rightMax = new int[length];

        for (int i = 1; i < length - 1; i++) {
            leftMax[i] = Math.max(leftMax[i-1], height[i-1]);
        }

        for (int i = length - 2; i >= 0; i--) {
            rightMax[i] = Math.max(rightMax[i+1], height[i+1]);
        }

        int sum = 0;
        for (int i = 1; i < length -1; i++) {
            int min = Math.min(leftMax[i], rightMax[i]);
            if(min > height[i]){
                sum += (min - height[i]);
            }
        }


        return sum;
    }
}
