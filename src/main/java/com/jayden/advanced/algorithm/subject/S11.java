package com.jayden.advanced.algorithm.subject;

/**
 * 盛最多水的容器
 *
 * @author by laijie
 * @Date 2021/4/21 上午11:35
 */
public class S11 {
    public int maxArea(int[] height) {

        int left = 0 ,right = height.length - 1;
        int ans = 0;
        while (left < right){
            int area = Math.min(height[left], height[right]) * (right - left);
            ans = Math.max(area, ans);

            if(height[left] > height[right]){
                right--;
            }else {
                left++;
            }
        }
        return ans;

    }
}
