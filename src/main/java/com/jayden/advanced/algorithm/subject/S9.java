package com.jayden.advanced.algorithm.subject;

/**
 * 9. 回文数
 *
 * @author by laijie
 * @Date 2021/4/21 上午11:04
 */
public class S9 {

    public static void main(String[] args) {
        S9 s9 = new S9();
        s9.isPalindrome(1651);
    }
    public boolean isPalindrome(int x) {

        if(x < 0 || (x % 10 == 0 && x != 0)){
            return false;
        }

        int revertedNum = 0;

        while (x > revertedNum){
            revertedNum = revertedNum * 10 +  x % 10;
            x = x / 10;
        }

        return revertedNum / 10 == x || revertedNum == x;
    }
}
