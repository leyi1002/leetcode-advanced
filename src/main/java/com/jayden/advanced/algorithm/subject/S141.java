package com.jayden.advanced.algorithm.subject;

import java.util.HashSet;

/**
 * 141. 环形链表
 *
 * @author by laijie
 * @Date 2021/4/23 上午10:55
 */
public class S141 {

    public boolean hasCycle(ListNode head) {

        ListNode slow = head;
        ListNode fast = head.next;

        while (fast != null && fast.next != null) {

            if (slow == fast) {
                return true;
            }
            slow = slow.next;
            fast = fast.next.next;
        }
        return false;
    }

    /**
     * hash表
     *
     * @param head
     * @return
     */
    public boolean hasCycle2(ListNode head) {
        HashSet<Object> hashSet = new HashSet<>();

        ListNode cur = head;
        while (cur != null) {
            if (!hashSet.add(cur)) {
                return true;
            }
            hashSet.add(cur);
            cur = cur.next;
        }
        return false;
    }
}
