package com.jayden.advanced.algorithm.subject;

/**
 * 206. 反转链表
 *
 * @author by laijie
 * @Date 2021/4/22 下午5:14
 */
public class S206 {

    public ListNode reverseList(ListNode head) {

        ListNode pre = null;
        ListNode next = head;
        while (next != null){
            ListNode temp = next.next;
            next.next = pre;
            pre = next;
            next = temp;

        }

        return pre;
    }
}
