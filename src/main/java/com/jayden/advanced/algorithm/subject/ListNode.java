package com.jayden.advanced.algorithm.subject;

/**
 * @author by laijie
 * @Date 2021/4/20 21:19
 */
public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
