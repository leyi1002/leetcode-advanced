package com.jayden.advanced.algorithm.subject;

/**
 * 19. 删除链表的倒数第 N 个结点
 * 1,2,3,4,5 2
 * @author by laijie
 * @Date 2021/4/21 上午11:47
 */
public class S19 {

    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        ListNode listNode5 = new ListNode(5);

        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;

        S19 s19 = new S19();
        ListNode listNode = s19.removeNthFromEnd(listNode1, 1);
        System.out.println(listNode);
    }

    public ListNode removeNthFromEnd(ListNode head, int n) {

        ListNode dummy = new ListNode(0, head);

        int length = 0;
        while (head != null){
            head = head.next;
            length++;
        }

        ListNode cur = dummy;
        for (int i = 1; i < length - n + 1; i++) {
            cur = cur.next;
        }

        cur.next = cur.next.next;

        return dummy.next;
    }
}
