package com.jayden.advanced.algorithm.sort;

import java.util.Arrays;

/**
 * 快速排序
 *
 * @author by laijie
 * @Date 2021/4/23 下午3:48
 */
public class QuickSort {

    public static void main(String[] args) {
        int[] arr = {11,44,23,67,88,65,34,48,9,12};
        quickSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    private static void  quickSort(int[] nums){
        if(nums == null || nums.length == 0 || nums.length == 1){
            return;
        }
        sort(nums, 0, nums.length-1);
    }

    private static void sort(int[] nums,int start, int end) {
        if(start >= end){
            return;
        }

        int base = nums[start];
        int left = start,right = end;

        while (left != right){

            while (nums[right] >= base && left < right){
                right--;
            }
            while (nums[left] <= base && left < right){
                left++;
            }

            if(left < right){
                int temp = nums[left];
                nums[left] = nums[right];
                nums[right] = temp;
            }
        }

        nums[start] = nums[left];
        nums[left]= base;

        sort(nums,start, left-1);
        sort(nums,left + 1,end);
    }

}
