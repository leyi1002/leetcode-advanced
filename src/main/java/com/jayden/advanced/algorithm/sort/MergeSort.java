package com.jayden.advanced.algorithm.sort;

/**
 * 归并排序
 *
 * @author by laijie
 * @Date 2021/4/23 下午3:46
 */
public class MergeSort {

    public static void main(String[] args) {
        int[] arr = {11,44,23,67,88,65,34,48,9,12};
        mergeSort(arr,0,arr.length-1);
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
    }

    private static void mergeSort(int[] arr, int low, int hign){
        if(low < hign){
            int mid = low + (hign - low)/2;
            mergeSort(arr,low, mid);
            mergeSort(arr,mid + 1, hign);
            merge(arr,low,hign,mid);
        }
    }

    private static void merge(int[] arr, int low, int hign, int mid) {

        int[] temp = new int[arr.length];
        int left =low,right=mid+1,index=0;
        while (left <= mid && right <= hign){
            if(arr[left] < arr[right]){
                temp[index++] = arr[left++];
            }else {
                temp[index++] = arr[right++];
            }
        }

        while (left <= mid){
            temp[index++] = arr[left++];
        }
        while (right <= hign){
            temp[index++] = arr[right++];
        }

        for (int i = 0; i < index; i++) {
            arr[low + i] =temp[i];
        }
    }

}
