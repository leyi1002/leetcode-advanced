package com.jayden.advanced.algorithm.sort;

import java.util.Arrays;

/**
 * 冒泡排序
 *
 * @author by laijie
 * @Date 2021/4/23 下午3:48
 */
public class BubbleSort {

    public static void main(String[] args) {
        int[] arr = {11,44,23,67,88,65,34,48,9,12};

        bubleSort(arr);

        System.out.println(Arrays.toString(arr));

    }

    private static void bubleSort(int[] arr) {

        for (int i = 0; i < arr.length-1; i++) {

            for (int j = 0; j < arr.length - 1 - i; j++) {
                if(arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }

    }

}
